package com.geolocation.controller;

import com.geolocation.entity.VehicleLightWeight;
import com.geolocation.model.CoordinateModel;
import com.geolocation.service.VehicleLightWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

@Controller
@RequestMapping("/vehicle-light-weigh")
public class VehicleLightWeightController {

    @Autowired
    private VehicleLightWeightService vehicleLightWeightService;

    @GetMapping("/search")
    public String vehicleLightWeightSearchPage() {
        return "vehicleLightWeightSearchPage";
    }

    @ResponseBody
    @GetMapping()
    public Collection<VehicleLightWeight> getPage(@RequestParam Double bottomLeftLat, @RequestParam Double bottomLeftLon,
                                                  @RequestParam Double upperRightLat, @RequestParam Double upperRightLon) {
        return vehicleLightWeightService.getInBoxOfCoordinate(bottomLeftLat, bottomLeftLon, upperRightLat, upperRightLon);
    }

    @ResponseBody
    @PutMapping()
    public ResponseEntity updateCarLog(@RequestBody CoordinateModel coordinateModel) {
        vehicleLightWeightService.save(coordinateModel);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/create_demo_vehicleLightWeigh")
    public String vehiclePage() {
        Double minLat = -90d;
        Double maxLat = 90d;
        Double minLon = -180d;
        Double maxLon = 180d;
        for (Long carId = 0l; carId < 1_000_000l; carId++) {
            Double lat = ThreadLocalRandom.current().nextDouble(minLat, maxLat);
            Double lon = ThreadLocalRandom.current().nextDouble(minLon, maxLon);
            vehicleLightWeightService.save(new CoordinateModel(carId, lat, lon, LocalDateTime.now()));
        }
        return "vehicleLightWeigh";
    }

}
