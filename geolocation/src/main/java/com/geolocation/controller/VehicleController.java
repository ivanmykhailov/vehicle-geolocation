package com.geolocation.controller;

import com.geolocation.entity.Vehicle;
import com.geolocation.model.CoordinateModel;
import com.geolocation.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

@Controller
@RequestMapping("vehicle")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("/search")
    public String vehicleSearchPage() {
        return "vehicleSearchPage";
    }

    @ResponseBody
    @GetMapping()
    public Collection<Vehicle> getPage(@RequestParam Double bottomLeftLat, @RequestParam Double bottomLeftLon,
                                       @RequestParam Double upperRightLat, @RequestParam Double upperRightLon) {
        LocalDateTime fromDate = LocalDateTime.now().minusHours(2);
        LocalDateTime toDate = LocalDateTime.now();
        return vehicleService.getInBoxOfCoordinate(fromDate, toDate, bottomLeftLat, bottomLeftLon, upperRightLat, upperRightLon);
    }

    @ResponseBody
    @PutMapping()
    public ResponseEntity updateCarLog(@RequestBody CoordinateModel coordinateModel) {
        coordinateModel.setLogTime(LocalDateTime.now());
        vehicleService.save(coordinateModel);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping("/create_demo_vehicle")
    public String vehiclePage() {
        Double minLat = -90d;
        Double maxLat = 90d;
        Double minLon = -180d;
        Double maxLon = 180d;
        for (int i = 0; i < 5; i++) {
            for (Long carId = 0l; carId < 100_000l; carId++) {
                Double lat = ThreadLocalRandom.current().nextDouble(minLat, maxLat);
                Double lon = ThreadLocalRandom.current().nextDouble(minLon, maxLon);
                vehicleService.save(new CoordinateModel(carId, lat, lon, LocalDateTime.now()));
            }
        }
        return "vehicle";
    }
}
