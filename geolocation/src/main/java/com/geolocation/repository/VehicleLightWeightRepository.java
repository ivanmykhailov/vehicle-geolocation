package com.geolocation.repository;

import com.geolocation.entity.VehicleLightWeight;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Box;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VehicleLightWeightRepository extends MongoRepository<VehicleLightWeight, Long> {
    List<VehicleLightWeight> findByLocationWithin(Box box);
    List<VehicleLightWeight> findByLocationWithin(Box box, Pageable pageable);
}
