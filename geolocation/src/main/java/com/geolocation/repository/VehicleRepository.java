package com.geolocation.repository;

import com.geolocation.entity.Vehicle;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Box;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface VehicleRepository extends MongoRepository<Vehicle, String> {
    List<Vehicle> findByLogTimeBetweenAndLocationWithin(LocalDateTime fromTime, LocalDateTime toTime, Box box);
    List<Vehicle> findByLogTimeBetweenAndLocationWithin(LocalDateTime fromTime, LocalDateTime toTime, Box box, Pageable pageable);

}
