package com.geolocation.service;

import com.geolocation.entity.VehicleLightWeight;
import com.geolocation.model.CoordinateModel;

import java.util.Collection;

public interface VehicleLightWeightService {

    void save(CoordinateModel coordinateModel);
    void save(Collection<CoordinateModel> coordinateModels);

    Collection<VehicleLightWeight> getInBoxOfCoordinate(Double bottomLeftLat,
                                                  Double bottomLeftLon,
                                                  Double upperRightLat,
                                                  Double upperRightLon);
}

