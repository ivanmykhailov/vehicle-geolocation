package com.geolocation.service;

import com.geolocation.entity.VehicleLightWeight;
import com.geolocation.model.CoordinateModel;
import com.geolocation.repository.VehicleLightWeightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class VehicleLightWeightServiceImpl implements VehicleLightWeightService {
    @Autowired
    private VehicleLightWeightRepository vehicleLightWeightRepository;

    @Override
    public void save(CoordinateModel coordinateModel){
        vehicleLightWeightRepository.save(new VehicleLightWeight(coordinateModel));
    }

    @Override
    public void save(Collection<CoordinateModel> coordinateModels){
        for (CoordinateModel coordinateModel: coordinateModels){
            save(coordinateModel);
        }
    }

    @Override
    public List<VehicleLightWeight> getInBoxOfCoordinate(Double bottomLeftLat, Double bottomLeftLon, Double upperRightLat, Double upperRightLon) {
        Point first = new Point(bottomLeftLat, bottomLeftLon);
        Point second = new Point(upperRightLat, upperRightLon);
        return vehicleLightWeightRepository.findByLocationWithin(new Box(first, second));
    }
}
