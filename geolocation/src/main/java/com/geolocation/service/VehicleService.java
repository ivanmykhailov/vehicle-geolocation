package com.geolocation.service;

import com.geolocation.entity.Vehicle;
import com.geolocation.model.CoordinateModel;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

public interface VehicleService {
    void save(CoordinateModel coordinateModel);
    void save(Collection<CoordinateModel> coordinateModels);
    Collection<Vehicle> getInBoxOfCoordinate(LocalDateTime fromTime, LocalDateTime toTime,
            Double bottomLeftLat, Double bottomLeftLon,
            Double upperRightLat, Double upperRightLon);
}
