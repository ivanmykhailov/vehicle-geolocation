package com.geolocation.service;

import com.geolocation.entity.Vehicle;
import com.geolocation.model.CoordinateModel;
import com.geolocation.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Override
    public void save(CoordinateModel coordinateModel) {
        vehicleRepository.save(new Vehicle(coordinateModel));
    }

    @Override
    public void save(Collection<CoordinateModel> coordinateModels) {
        for (CoordinateModel coordinateModel: coordinateModels){
            save(coordinateModel);
        }
    }

    @Override
    public Collection<Vehicle> getInBoxOfCoordinate(LocalDateTime fromTime, LocalDateTime toTime, Double bottomLeftLat, Double bottomLeftLon, Double upperRightLat, Double upperRightLon) {
        Point first = new Point(bottomLeftLat, bottomLeftLon);
        Point second = new Point(upperRightLat, upperRightLon);
        List<Vehicle> vehicles = vehicleRepository.findByLogTimeBetweenAndLocationWithin(fromTime, toTime, new Box(first, second));
        Collections.sort(vehicles, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle o1, Vehicle o2) {
                return o1.getLogTime().compareTo(o2.getLogTime());
            }
        });
        Map<Long, Vehicle> vehicleMap = new HashMap<>();
        for (Vehicle vehicle: vehicles){
            vehicleMap.put(vehicle.getCarId(), vehicle);
        }
        return vehicleMap.values();
    }
}
