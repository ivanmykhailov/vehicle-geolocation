package com.geolocation.util;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateUtil {
    public static Long getMilliSecondsByLocalDateTime(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
