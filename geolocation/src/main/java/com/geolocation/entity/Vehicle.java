package com.geolocation.entity;

import com.geolocation.model.CoordinateModel;
import com.geolocation.util.DateUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@CompoundIndex(def = "{'location': 1, 'log_time': 1}")
@Document(collection = "vehicle")
public class Vehicle {
    @Id
    private String id;

    @Field("car_id")
    private Long carId;

    private GeoJsonPoint location;

    @Field("log_time")
    private LocalDateTime logTime;

    public Vehicle() {
    }

    public Vehicle(String id) {
        this.id = id;
    }

    public Vehicle(CoordinateModel coordinateModel) {
        this.id = coordinateModel.getCarId() + "_" + DateUtil.getMilliSecondsByLocalDateTime(coordinateModel.getLogTime());
        this.carId = coordinateModel.getCarId();
        this.location = new GeoJsonPoint(coordinateModel.getLat(), coordinateModel.getLon());
        this.logTime = coordinateModel.getLogTime();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public LocalDateTime getLogTime() {
        return logTime;
    }

    public void setLogTime(LocalDateTime logTime) {
        this.logTime = logTime;
    }
}
