package com.geolocation.entity;

import com.geolocation.model.CoordinateModel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "vehicle_light_weight")
public class VehicleLightWeight {
    @Id
    @Field("car_id")
    private Long carId;

    @Indexed
    private GeoJsonPoint location;

    public VehicleLightWeight() {
    }

    public VehicleLightWeight(CoordinateModel coordinateModel) {
        this.carId = coordinateModel.getCarId();
        this.location = new GeoJsonPoint(coordinateModel.getLat(), coordinateModel.getLon());

    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }
}
