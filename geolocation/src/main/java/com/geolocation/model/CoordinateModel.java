package com.geolocation.model;

import java.time.LocalDateTime;

public class CoordinateModel {
    private Long carId;
    private Double lat;
    private Double lon;
    private LocalDateTime logTime;
    public CoordinateModel() {}

    public CoordinateModel(Long carId, Double lat, Double lon, LocalDateTime logTime) {
        this.carId = carId;
        this.lat = lat;
        this.lon = lon;
        this.logTime = logTime;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public LocalDateTime getLogTime() {
        return logTime;
    }

    public void setLogTime(LocalDateTime logTime) {
        this.logTime = logTime;
    }
}
