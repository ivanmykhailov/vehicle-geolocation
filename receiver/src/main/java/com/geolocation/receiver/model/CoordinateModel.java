package com.geolocation.receiver.model;

import java.util.Objects;

public class CoordinateModel {
    private Long carId;
    private  Double lat;
    private Double lon;
    private Integer hashCode;
    public CoordinateModel() {}

    public CoordinateModel(Long carId, Double lat, Double lon) {
        this.carId = carId;
        this.lat = lat;
        this.lon = lon;
        this.hashCode = carId.hashCode();
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
        this.hashCode = this.carId.hashCode();
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinateModel that = (CoordinateModel) o;
        return Objects.equals(carId, that.carId);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "{" +
                "carId=" + carId +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
