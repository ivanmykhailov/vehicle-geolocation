package com.geolocation.receiver;

import com.geolocation.receiver.service.TemporaryStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class GeolocationApplication implements CommandLineRunner {


	@Autowired
	private TemporaryStorageService temporaryStorageService;

	public static void main(String[] args) {
		SpringApplication.run(GeolocationApplication.class, args);
	}

	@Override
	public void run(String... args) {
		temporaryStorageService.run();
	}
}
