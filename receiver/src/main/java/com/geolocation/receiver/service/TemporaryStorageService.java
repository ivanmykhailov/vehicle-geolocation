package com.geolocation.receiver.service;

import com.geolocation.receiver.model.CoordinateModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

@Service
public class TemporaryStorageService {

    @Value("${com.geolocation.main.server.url}")
    private String mainServerUrl;

    private static ConcurrentLinkedQueue<CoordinateModel> concurrentLinkedQueue = new ConcurrentLinkedQueue<CoordinateModel>();

    public void addCoordinateModel(CoordinateModel coordinateModel) {
        concurrentLinkedQueue.add(coordinateModel);
    }

    @Async
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {}
            Set<CoordinateModel> coordinateModelSet = new HashSet<>();
            int queueSize = concurrentLinkedQueue.size();
            for (int i = 0; i < queueSize; i++) {
                coordinateModelSet.add(concurrentLinkedQueue.poll());
            }
            if(!coordinateModelSet.isEmpty()) {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.put(mainServerUrl, coordinateModelSet);
            }
        }
    }
}
