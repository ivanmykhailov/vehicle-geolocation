package com.geolocation.receiver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

    @GetMapping("/test-page")
    public String testPage() {
        return "loadingDataTestPage";
    }
}
