package com.geolocation.receiver.controller;

import com.geolocation.receiver.model.CoordinateModel;
import com.geolocation.receiver.service.TemporaryStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("geolocation")
public class GeolocationController {

    @Autowired
    private TemporaryStorageService temporaryStorageService;

    @ResponseBody
    @PutMapping()
    public ResponseEntity updateCarLog(@RequestBody CoordinateModel coordinateModel) {
        temporaryStorageService.addCoordinateModel(coordinateModel);
        return new ResponseEntity(HttpStatus.OK);
    }
}
