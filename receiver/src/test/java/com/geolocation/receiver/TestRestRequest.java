package com.geolocation.receiver;

import com.geolocation.receiver.model.CoordinateModel;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class TestRestRequest {

    public static void main(String[] args) throws IOException {
        String uri = "http://localhost:8070/geolocation";
        Double minLat = -90d;
        Double maxLat = 90d;
        Double minLon = -180d;
        Double maxLon = 180d;
        for (Long carId = 0l; carId < 10_000l; carId++) {
            Double lat = ThreadLocalRandom.current().nextDouble(minLat, maxLat);
            Double lon = ThreadLocalRandom.current().nextDouble(minLon, maxLon);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(uri, new CoordinateModel(carId, lat, lon));
        }
    }

}
